from youtube_utils.ytutils import YoutubeManager


def test_get_video_id():
    # Stephanie Sykes @ Monasterio Factory, June 2019 | BE-AT.TV Rewind
    ym = YoutubeManager()
    url = "https://youtu.be/4utKO75DtBE"
    video_id = ym.get_video_id(url)
    assert video_id == "4utKO75DtBE"

    url = "https://www.youtube.com/watch?v=4utKO75DtBE&ab_channel=BE-AT.TV"
    video_id = ym.get_video_id(url)
    assert video_id == "4utKO75DtBE"


def test_get_metadata():
    # Stephanie Sykes @ Monasterio Factory, June 2019 | BE-AT.TV Rewind
    ym = YoutubeManager()
    video_id = "4utKO75DtBE"
    metadata = ym.get_metadata(video_id)
    assert metadata["name"] == "Stephanie Sykes @ Monasterio Factory, June 2019 | BE-AT.TV Rewind"
    assert metadata["channel"]["name"] == "BE-AT.TV"


def test_get_yt_tracks_from_video():
    # Amelie Lens - Ultra Music Festival - Miami 2022
    ym = YoutubeManager()
    video_id = "mw4YDAd9qmo"
    yt_tracks = ym._get_yt_tracks_from_video(video_id)
    assert len(yt_tracks) == 10
    assert yt_tracks[0]["title"] == "Newoldgen (Marcal Remix)"
    assert yt_tracks[0]["artist"] == "Coyu"
    assert yt_tracks[0]["album"] == "Post Raw Era Remixes Part II"
    a = 2


def test_get_ct_tracks_from_video():
    ym = YoutubeManager()
    video_id = "mw4YDAd9qmo"
    comments = ym._get_ct_tracks_from_video(video_id)
    a = 2


def test_get_youtube_tracks():
    pass
