from ytutils import YoutubeManager
from spotify_utils import SpotifyManager
import csv

yt = YoutubeManager()
sp = SpotifyManager()


def test_create_some_playlists():
    url_list = []
    with open("misc/sets.csv") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        next(csv_reader)
        for row in csv_reader:
            url = row[1]
            vdict = yt.get_tracks_from_set(url)
            sp.create_playlist_from_youtube_set(vdict)


def test_create_single_playlist():
    # url = 'https://www.youtube.com/watch?v=4utKO75DtBE'
    url = "https://www.youtube.com/watch?v=d2XTF3gTCGE"
    url = "https://www.youtube.com/watch?v=mzwAC_Am0bI"
    url = "https://www.youtube.com/watch?v=91NTrpx9Rqw"

    vdict = yt.get_tracks_from_set(url)
    sp.create_playlist_from_youtube_set(vdict)


def test_createSimilarPlaylist():
    playlist_url = "https://open.spotify.com/playlist/6ttSHCOkXf7qb3EatdTBBE?si=a21c544540434f94"
    playlist_id = playlist_url[34:56]

    opts = {"album": 3, "artist": 2, "include": 0}
    print("\n")
    sp.createSimilarPlaylist(playlist_id, opts)
