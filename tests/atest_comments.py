import pytest
import csv
from ytutils import YoutubeManager
from spotify_utils import SpotifyManager

# from myutils import check_line

yt = YoutubeManager()
sp = SpotifyManager()


def readFile(name):
    with open(f"tests/comments/{name}.txt", encoding="utf-8") as f:
        return f.read()


@pytest.fixture
def get_video_comments():
    data = []
    with open("misc/sets.csv") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        next(csv_reader)
        for row in csv_reader:
            title = row[0]
            url = row[1]
            file_name = row[2]
            data.append({"title": title, "url": url, "comment": [readFile(file_name)]})
    return data


def test_detect_comments(get_video_comments):
    for data in get_video_comments:
        url = data["url"]
        comments_check = data["comment"]

        vdict = yt.get_tracks_from_set(url)
        comments_found = vdict["comment"]
        assert any(x in comments_check for x in comments_found)
        assert any(x in comments_found for x in comments_check)


def test_format_comments(get_video_comments):
    for data in get_video_comments:
        comment = data["comment"][0].split("\n")
        commment_formated = sp.formatCtTracks(comment)
        # comment_formated = list(filter(None, list(map(check_line, comment))))

        a = 2


def test_format_single_comment():
    file_name = "lady"
    comment = readFile(file_name).split("\n")
    comment_formated = list(filter(None, list(map(check_line, comment))))
    a = 2


def test_format_single_line():
    line = "47:45 Mode_1 - Offtrack"
    # line = '52:21 DA/FR - Charlotte Pipe (The Lady Machine Remix)'
    line_formatted = check_line(line)
    a = 2


def test_get_comments_from_set():
    url = "https://www.youtube.com/watch?v=4utKO75DtBE"
    vdict = yt.get_tracks_from_set(url)
