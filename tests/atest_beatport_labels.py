from ytutils import YoutubeManager
from spotify_utils.sputils import SpotifyManager

# from bputils import getTopTracksFromLabel

sp = SpotifyManager()
yt = YoutubeManager()


def test_get_labels_from_playlist():
    playlist_url = "https://open.spotify.com/playlist/7HaBMQiY8vfpDbn2lyCB3y?si=ada0c2eb5098464e"
    playlist_id = playlist_url[34:56]
    labels = sp.getLabelsFromPlaylist(playlist_id)

    print("\n")
    for l in labels:
        print(l)


def test_create_playlist_from_label():
    label = "BLACKAXON"
    sp.createPlaylistFromLabel(label)


def test_create_playlist_labels_from_set():
    url = "https://www.youtube.com/watch?v=M0yGNYk5cZY"
    vdict = yt.get_tracks_from_set(url)
    playlist = sp.create_playlist_from_youtube_set(vdict)
    labels = sp.getLabelsFromPlaylist(playlist["id"])
    for l in labels:
        sp.createPlaylistFromLabel(l)


def test_get_top_tracks_from_label():
    label = "polegroup"
    tracks = getTopTracksFromLabel(label)
    a = 2
