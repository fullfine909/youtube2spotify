from spotify_utils import SpotifyManager


def test_delete():
    sp = SpotifyManager()
    playlists = sp.getPlaylists()
    sp.delete_all_playlists(playlists)
