from spotify_utils import SpotifyManager


def test_get_single_track_formatted():
    sp = SpotifyManager()

    # track = 'kay d smith person called'
    # track = 'mode_1 offtrack'
    track = "dafr charlotte pipe the lady machine remix"
    track = "Kwartz - Counterbalance"
    t = sp.getSpotifyTrack(track)
    print("ey")


def test_get_single_track_unformatted():
    sp = SpotifyManager()

    # track = 'kay d smith person called'
    # track = 'mode_1 offtrack'
    # track = '18. Clouds - Chained To A Dead Camel (Overlee Assembyl)'
    track = "00:36:25 - Kay D Smith - Person called (Original Mix)"
    track = "Kwartz - Counterbalance"
    track_f = sp.formatCtTracks([track])[0]
    t = sp.getSpotifyTrack(track_f)
    print("ey")
