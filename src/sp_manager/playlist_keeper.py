from logger import logging
from datetime import datetime

from spotify_utils.sputils import SpotifyManager
from mongosp_utils.mgsputils import MongoSPManager


logger = logging.getLogger("root")


class PlaylistKeeper:
    def __init__(self, sm=None, md=None) -> None:
        self.name = "playlist_keeper"
        # SpotifyManager
        if sm:
            self.sm = sm
        else:
            self.sm = SpotifyManager()
        # MongoSPManager
        if md:
            self.md = md
        else:
            self.md = MongoSPManager()

    # 1. KEEP PLAYLIST
    def _insert_keep_playlist(self, playlist):
        self.md.insert_one(playlist, "Playlist Keep")

    def _get_updates(self, sp_tracks, mg_tracks):
        new_tracks = []
        mg_tracks_id = [t["id"] for t in mg_tracks]
        for sptrack in sp_tracks:
            if sptrack["id"] not in mg_tracks_id:
                new_tracks.append(sptrack)

        remove_tracks = []
        sp_tracks_id = [t["id"] for t in sp_tracks]
        for mgtrack in mg_tracks:
            if mgtrack["id"] not in sp_tracks_id:
                remove_tracks.append(mgtrack)

        updates = {"new_tracks": new_tracks, "remove_tracks": remove_tracks}
        return updates

    def _update_removed_tracks(self, playlist, remove_tracks):
        # update removed tracks
        for track in remove_tracks:
            query = {
                "track": track["id"],
                "playlist": playlist["id"],
            }

            values = {
                "$set": {"removed_at": datetime.today().replace(microsecond=0), "active": False}
            }
            self.md.collection.update_one(query, values)
            self.md.collection.find_one(query)

    # 2. GET PLAYLIST BY DATE
    def _get_date(self, date):
        self.date = datetime(2022, 7, 17, 13)

    def _get_tracks2playlist(self):
        self.md._check_collection_change("tracks2playlists")
        tracks2playlists = []
        for t2p in self.md.collection.find():
            tracks2playlists.append(t2p)
        self.tracks2playlist = tracks2playlists

    def _get_tracks_by_date(self, date):
        self.md._check_collection_change("tracks2playlists")
        query = {
            "$and": [
                {"added_at": {"$lte": date}},  # added < date
                {
                    "$or": [  # if active not True, check removed
                        {"active": True},
                        {"removed_at": {"$gte": date}},  # removed > date
                    ]
                },
            ]
        }

        results = self.md.collection.find(query)
        tracks_date = []
        for r in results:
            tracks_date.append(r)

        return tracks_date

    def _create_playlist(self, tracks_date):
        date_string = self.date.strftime("%d-%m-%Y")
        name = f'{self.playlist["name"]} ({date_string})'
        tracks = [t["track"] for t in tracks_date]
        playlist = self.sm.create_playlist(name, tracks)
        print(playlist["external_urls"]["spotify"])

    # 1. KEEP PLAYLIST
    def keep_playlist(self, query, input_format):
        playlist = self.sm.get_sp_playlist(query, input_format)
        sp_tracks = self.sm.get_tracks_from_sp_playlist(playlist)
        mg_tracks = self.md.get_tracks_from_mgdb_playlist(playlist)
        updates = self._get_updates(sp_tracks, mg_tracks)
        if not mg_tracks:
            self.md.insert_playlist(playlist)
            self._insert_keep_playlist(playlist)
        self.md.insert_tracks(updates["new_tracks"])
        self.md.insert_tracks2playlist(playlist, updates["new_tracks"])
        self._update_removed_tracks(playlist, updates["remove_tracks"])
        logger.info(f"Playlist {playlist['id']}")

    # 2. GET PLAYLIST BY DATE
    def get_playlist_by_date(self, query, input_format, input_date):
        playlist = self.sm.get_sp_playlist(query, input_format)
        date = self._get_date(input_date)
        self._get_tracks2playlist()
        self._get_tracks_by_date()
        self._create_playlist()

    # 3. LIST PLAYLISTS
    def list_playlists(self):
        self.md.list_records("Playlists Keep")
        # self.md.list_records("Tracks")

    # 4. LIST TRACKS IN PLAYLIST
    def list_tracks_in_playlist(self):
        pass

    # 5. RESTART
    def restart_playlists(self):
        self.md.delete_collection("Playlists Keep")

    # 6. AUTO UPDATE PLAYLISTS (every week)
    def update_playlists(self):
        playlists = self.md.get_playlists()
        for playlist in playlists:
            self.keep_playlist(playlist["id"], "id")
