from spotify_utils.sputils import SpotifyManager
from mongosp_utils.mgsputils import MongoSPManager
from datetime import datetime


class PlaylistKeeper:
    def __init__(self) -> None:
        self.sp = SpotifyManager()
        self.md = MongoSPManager()
        self.md.set_database("playlist_keep")

    # User input
    def get_playlist(self, query, input_format):
        if input_format == "-n":
            playlist = self.sp.get_spotify_playlist_by_name(query)
        elif input_format == "-i":
            playlist = self.sp.get_playlist_by_id(query)
        else:
            raise Exception("Input format different from -n (name) or -i (id)")
        self.playlist = playlist
        return playlist

    def get_tracks_from_playlist(self, playlist):
        tracks = self.sp.get_tracks_from_playlist_id(playlist["id"])
        tracks_formatted = list(map(self.sp._format_spotify_track, tracks))
        # tracks_active = [dict(item, active=True) for item in tracks_formatted]
        self.input_tracks = tracks_formatted

    def get_tracks_from_mgdb(self):
        self.md._check_collection_change("Tracks")
        pipeline = [
            {
                "$lookup": {
                    "from": "tracks2playlists",
                    "localField": "id",
                    "foreignField": "track",
                    "as": "playlist_status",
                }
            },
            {"$match": {"playlist_status.playlist": self.playlist["id"]}},
            {"$match": {"playlist_status.active": True}},
        ]
        full_tracks_response = self.md.collection.aggregate(pipeline)
        full_tracks_data = []
        while full_tracks_response._has_next():
            track_data = full_tracks_response.next()
            playlists_status = track_data.pop("playlist_status")
            track_data["active"] = playlists_status[0]["active"]
            track_data["added_at"] = playlists_status[0]["added_at"]
            track_data["removed_at"] = playlists_status[0]["removed_at"]
            full_tracks_data.append(track_data)
        self.mgdb_tracks = full_tracks_data

    def insert_playlist(self):
        if len(self.mgdb_tracks) == 0:
            self.md.insert_one(self.playlist, "Playlists")

    def insert_tracks(self):
        self.md.insert_records(self.new_tracks, "Tracks")

    def get_updates(self):
        new_tracks = []
        mgdb_tracks_id = [t["id"] for t in self.mgdb_tracks]
        for itrack in self.input_tracks:
            if itrack["id"] not in mgdb_tracks_id:
                new_tracks.append(itrack)
        self.new_tracks = new_tracks

        update_tracks = []
        input_tracks_id = [t["id"] for t in self.input_tracks]
        for mgtrack in self.mgdb_tracks:
            if mgtrack["id"] not in input_tracks_id:
                update_tracks.append(mgtrack)
        self.update_tracks = update_tracks

    def get_tracks2playlist(self):
        self.md._check_collection_change("tracks2playlists")
        tracks2playlists = []
        for t2p in self.md.collection.find():
            tracks2playlists.append(t2p)
        self.tracks2playlist = tracks2playlists

    def insert_tracks2playlist(self):
        # new tracks
        tracks_in_playlist = []
        for t in self.new_tracks:
            date_string = datetime.strptime(t.pop("added_at"), "%Y-%m-%dT%H:%M:%SZ")
            date = date_string.replace(microsecond=0)
            tip = {
                "track": t["id"],
                "playlist": self.playlist["id"],
                "added_at": date,
                "removed_at": None,
                "active": True,
            }
            tracks_in_playlist.append(tip)
        self.md.insert_records(tracks_in_playlist, "tracks2playlists")

        # update removed tracks
        for t in self.update_tracks:
            tip = {
                "track": t["id"],
                "playlist": self.playlist["id"],
                "removed_at": "today",
                "active": False,
            }
            query = {"track": tip["track"], "playlist": tip["playlist"]}
            values = {
                "$set": {"removed_at": datetime.today().replace(microsecond=0), "active": False}
            }
            self.md.collection.update_one(query, values)
            self.md.collection.find_one(query)

    def get_date(self, date):
        self.date = datetime(2022, 7, 17, 13)

    def get_tracks_by_date(self):
        self.md._check_collection_change("tracks2playlists")
        query = {
            "$and": [
                {"added_at": {"$lte": self.date}},  # added < date
                {
                    "$or": [  # if active not True, check removed
                        {"active": True},
                        {"removed_at": {"$gte": self.date}},  # removed > date
                    ]
                },
            ]
        }

        results = self.md.collection.find(query)
        rarray = []
        for r in results:
            rarray.append(r)

        self.tracks_date = rarray

    def create_playlist(self):
        date_string = self.date.strftime("%d-%m-%Y")
        name = f'{self.playlist["name"]}: {date_string}'
        tracks = [t["track"] for t in self.tracks_date]
        playlist = self.sp.create_playlist(name, tracks)
        print(playlist["external_urls"]["spotify"])

    # Method 1
    def keep_playlist(self, query, input_format):
        playlist = self.get_playlist(query, input_format)
        self.get_tracks_from_playlist(playlist)
        self.get_tracks_from_mgdb()
        self.get_updates()
        self.insert_playlist()
        self.insert_tracks()
        self.insert_tracks2playlist()

    # # Method 2
    # def get_playlist_by_date(self, playlist_input, input_date):
    #     self.get_playlist(playlist_input)
    #     self.get_date(input_date)
    #     self.get_tracks2playlist()
    #     self.get_tracks_by_date()
    #     self.create_playlist()

    # # Method 3
    # def list_collections(self):
    #     self.md.list_records("Playlists")
    #     self.md.list_records("Tracks")

    # # Method 4
    # def restart_collections(self):
    #     self.md.delete_collection("Playlists")
    #     self.md.delete_collection("Tracks")
    #     self.md.delete_collection("tracks2playlists")
    #     self.md.create_index("id", "Playlists")
    #     self.md.create_index("id", "Tracks")


def main():
    pk = PlaylistKeeper()
    pk.start()


if __name__ == "__main__":
    main()
