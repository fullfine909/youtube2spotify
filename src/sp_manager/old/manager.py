from abc import ABC, abstractclassmethod
from spotify_utils.sputils import SpotifyManager
from yutils.ytutils import YoutubeManager
from bputils import BeatportManager
from logg import logger


# METHOD 1: YOUTUBE TO SPOTIFY
# METHOD 2: LABEL TOP TRACKS
# METHOD 3: LABEL TOP TRACKS (try inherit from Method 2)


class MethodManager(ABC):
    def __init__(self, source, opts):
        self.source = source
        self.opts = opts
        self.sp = SpotifyManager()
        self.yt = YoutubeManager()
        self.bp = BeatportManager()

    def create_playlist(self):
        name = self.get_playlist_name()
        playlist = self.sp.get_spotify_playlist_by_name(name)

        if playlist and self.opts["write"] == False:
            return playlist
        else:
            if playlist:
                self.sp.delete_playlist(playlist["id"])
            tracks = self.get_playlist_tracks()
            playlist = self.sp.create_playlist(name, tracks)
            playlist = self.sp.get_playlist_by_id(playlist["id"])
            return playlist

    def read_playlist(self, playlist):
        logger.info(f"Reading playlist: {playlist['name']}")
        for t in playlist["tracks"]["items"]:
            logger.info(f"{t['track']['name']} - {t['track']['artists'][0]['name']}")
        logger.info(f"Playlist url: {playlist['external_urls']['spotify']}")

    @abstractclassmethod
    def get_playlist_name(self):
        pass

    @abstractclassmethod
    def get_playlist_tracks(self):
        pass


# MAIN METHODS
# METHOD 1: YOUTUBE TO SPOTIFY
class Youtube2Spotify(MethodManager):
    def __init__(self, source, opts):
        super().__init__(source, opts)
        self.vid = self.yt.get_video_id(source)  # youtube url

    def get_playlist_name(self):
        metadata = self.yt.get_metadata(self.vid)
        return metadata["name"]

    def get_playlist_tracks(self):
        youtube_tracks = self.yt.get_youtube_tracks(self.vid)
        spotify_tracks = self.sp.get_spotify_tracks_from_youtube_tracks(youtube_tracks)
        return spotify_tracks


# METHOD 2: LABEL TOP TRACKS
class LabelTopTracks(MethodManager):
    def __init__(self, source, opts):
        super().__init__(source, opts)
        self.label_name = source  # label name
        self.lid = self.bp.get_beatport_label_id(self.label_name)

    def get_playlist_name(self):
        name = f"Beatport Top Tracks: {self.label_name}"
        return name

    def get_playlist_tracks(self):
        beatport_tracks = self.bp.get_beatport_top_tracks(self.lid)
        spotify_tracks = self.sp.get_spotify_tracks_from_beatport_list(beatport_tracks)
        return spotify_tracks


# METHOD 3: LABEL FULL TRACKS (try inherit from Method 2)
class LabelFullTracks(MethodManager):
    def __init__(self, source, opts):
        super().__init__(source, opts)
        self.label_name = source  # label name
        self.lid = self.bp.get_beatport_label_id(self.label_name)

    def get_playlist_name(self):
        name = f"Beatport Full Tracks: {self.label_name}"
        return name

    def get_playlist_tracks(self):
        beatport_tracks = self.bp.get_beatport_full_tracks(self.lid)
        spotify_tracks = self.sp.get_spotify_tracks_from_beatport_list(beatport_tracks)
        return spotify_tracks
