from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.middleware.cors import CORSMiddleware
from yutils.ytutils import YoutubeManager
from spotify_utils import SpotifyManager

app = FastAPI()

origins = ["*"]

add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

templates = Jinja2Templates(directory="app/templates")
yt = YoutubeManager()
sp = SpotifyManager()


@get("/yt", response_class=HTMLResponse)
async def home(request: Request):
    return templates.TemplateResponse("home.html", context={"request": request})


@post("/yt", response_class=HTMLResponse)
async def home3(request: Request, link_input: str = Form(...)):
    video_metadata = yt.get_tracks_from_set(link_input)
    playlist = sp.create_playlist_from_youtube_set(video_metadata)
    tracks = sp.get_tracks_data_from_sp(playlist)
    video_metadata["spt_url"] = playlist["external_urls"]["spotify"]
    video_metadata["name"] = playlist["name"]

    return templates.TemplateResponse(
        "result.html",
        context={"request": request, "arr": tracks, "video": video_metadata},
    )


# @post("/yt", response_class=HTMLResponse)
# async def home3(request: Request, link_input: str = Form(...)):
#     return templates.TemplateResponse("home2.html", context = {"request": request, 'link_display':link_input})

# uvicorn jinja_main:app --reload --host 0.0.0.0 --port 8080
# http://127.0.0.1:8080/yt
