from spotify_utils.sputils import SpotifyManager
from my_utils import write_csv
from my_utils import save_var
from my_utils import load_var


class Likes:
    def write_csv(tracks):
        lines = [["Title", "Artist", "Genres"]]
        for t in tracks:
            title = t["name"]
            artists = ", ".join([a["name"] for a in t["artists"]])
            genres = ", ".join([g for g in t["genres"]])
            lines.append([title, artists, genres])

        write_csv("test.csv", lines)

    def create_group(name):
        return {"name": name, "tracks": []}

    def classify(tracks):
        names = ["techno", "indie", "house", "rap", "pop", "hip hop"]
        groups = list(map(create_group, names))
        ow = []

        for t in tracks:
            check = False
            for genre_track in t["genres"]:
                for g in groups:
                    if g["name"] in genre_track:
                        g["tracks"].append(t)
                        check = True
            if check == False:
                ow.append(t)

        groups.append(create_group("ow"))
        groups[-1]["tracks"] = ow
        a = 2


def main():
    sp_manager = SpotifyManager()
    tracks = sp_manager.get_user_liked_tracks(10)
    sp_manager.add_artists_to_tracks(tracks)
    sp_manager.get_genres_from_tracks(tracks)
    save_var(tracks)

    tracks = load_var()
    classify(tracks)


if __name__ == "__main__":
    main()
