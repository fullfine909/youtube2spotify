from logger import logging

from spotify_utils.sputils import SpotifyManager
from youtube_utils.ytutils import YoutubeManager
from mongosp_utils.mgsputils import MongoSPManager
from mongosp_utils.constants import YOUTUBE_SETS

# from my_utils.myutils import load_var, save_var

logger = logging.getLogger("root")


class Youtube2Spotify:
    def __init__(self, sm=None, ym=None, md=None) -> None:
        self.name = "youtube_spotify"
        # SpotifyManager
        if sm:
            self.sm = sm
        else:
            self.sm = SpotifyManager()
        # YoutubeManager
        if ym:
            self.ym = ym
        else:
            self.ym = YoutubeManager()
        # MongoSPManager
        if md:
            self.db = md
        else:
            self.db = MongoSPManager()

    def _get_spotify_tracks_from_youtube_tracks(self, tracks):
        # youtube tracks
        yt_tracks = tracks["youtube_tracks"]
        yt_tracks_found = self.sm.search_tracks_in_spotify(yt_tracks)

        # comment tracks
        cm_tracks = tracks["comment_tracks"]
        cm_tracks_found = self.sm.search_tracks_in_spotify(cm_tracks)

        # merge sources
        spotify_tracks = self._merge_tracks(yt_tracks_found, cm_tracks_found)
        return spotify_tracks

    @staticmethod
    def _merge_tracks(yt_tracks, cm_tracks):
        # MARK YOUTUBE TRACKS THAT ARE IN COMMENTS
        for ytrack in yt_tracks:
            for c in cm_tracks:
                if ytrack["id"] == c["id"]:
                    ytrack["found"] = True
                    break

        # ADD MISSING YOUTUBE TRACKS TO COMMENTS TRACK LIST
        for idx, ytrack in enumerate(yt_tracks):
            if ytrack.get("found", False) == False:
                # if first youtube track hasn't been found, insert in first place
                if idx == 0:
                    insert_idx = 0
                else:
                    # get id of previous found track
                    prev_track_id = yt_tracks[idx - 1]["id"]
                    insert_idx = [c["id"] for c in cm_tracks].index(prev_track_id)
                # INSERT YT TRACK
                cm_tracks.insert(insert_idx + 1, ytrack)

        return cm_tracks

    def _get_playlist_from_video(self, video_id):
        video = self.db.get_record_by_id(video_id, YOUTUBE_SETS)
        if video:
            playlist_id = video["playlist_id"]
            playlist = self.db.get_playlist_by_id(playlist_id)
            return playlist
        return None

    #   1. YOUTUBE 2 SPOTIFY
    def youtube_2_spotify(self, youtube_url, write=False):
        video_id = self.ym.get_video_id(youtube_url)
        db_playlist = self._get_playlist_from_video(video_id)

        if not db_playlist or write:
            # get spotify tracks from youtube video_id
            youtube_tracks = self.ym.get_youtube_tracks(video_id)
            spotify_tracks = self._get_spotify_tracks_from_youtube_tracks(youtube_tracks)

            # create/delete&create playlist from spotify_tracks
            video_metadata = self.ym.get_metadata(video_id)
            playlist_name = video_metadata["name"]
            playlist = self.sm.create_playlist_write(db_playlist, playlist_name, spotify_tracks)
            video_metadata["playlist_id"] = playlist["id"]

            # # insert/update playlist and video
            self.db.insert_playlist(db_playlist, playlist)
            self.db.insert_video(video_metadata)

        else:
            playlist = db_playlist

        print("end")
        return playlist
