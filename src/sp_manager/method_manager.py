import fire
from sp_manager.youtube2spotify import Youtube2Spotify
from sp_manager.playlist_keeper import PlaylistKeeper

from spotify_utils.sputils import SpotifyManager
from youtube_utils.ytutils import YoutubeManager
from beatport_utils.bputils import BeatportManager
from mongosp_utils.mgsputils import MongoSPManager

from sp_manager.logg import configure_logger


configure_logger()


def return3():
    return 3


class MethodManager:
    def __init__(self):
        # API CONNECTIONS
        self.sm = SpotifyManager()
        self.ym = YoutubeManager()
        self.bm = BeatportManager()
        self.md = MongoSPManager()

        # DATA MANAGERS
        self.ys = Youtube2Spotify(sm=self.sm, ym=self.ym, md=self.md)
        self.pk = PlaylistKeeper(sm=self.sm, md=self.md)

    ##  YOUTUBE 2 SPOTIFY ##
    #   1. YOUTUBE URL TO SPOTIFY PLAYLIST
    def youtube2spotify(self, youtube_url, write=None):
        """
        Get a spotify playlist from a youtube set
        :param youtube_url: youtube url containing a dj set
        """
        playlist = self.ys.youtube_2_spotify(youtube_url, write)
        self.sm.display_playlist(playlist)

    def test_youtube2spotify(self, filename, write=None):
        """
        Get a spotify playlist from a youtube set
        :param youtube_url: youtube url containing a dj set
        """
        # self.ys.youtube_2_spotify(youtube_url)
        self.ys.test_youtube_2_spotify(filename, write)

    ##  BEATPOR LABELS ##
    #   1. GET LABEL TOP 10 PLAYLIST
    def get_label_top10(self, label_name):
        """
        Get
        """
        playlist = self.bm.get_top10_label_tracks(label_name)
        self.sm.display_playlist(playlist)

    ##  MONGO MANAGER ##
    def list_mongo_records(self, collection):
        self.md.list_records(collection)

    def list_mongo_collections(self):
        self.md.list_collections()

    def list_mongo_databases(self):
        self.md.list_databases()

    def reset_mongo_y2s(self):
        collections = ["playlists", "tracks", "youtube_sets"]
        for c in collections:
            self.md.delete_collection(c)

    def get_duplicated_tracks(self):
        self.md.get_duplicated_tracks()

    ##  SPOTIFY MANAGER ##
    def list_spotify_playlists(self):
        playlists = self.sm.get_user_playlists()
        for p in playlists:
            print(f'{p["name"]}: {p["id"]}')

    def reset_spotify_library(self):
        self.sm.delete_all_playlists()

    ##  PLAYLIST KEEPER ##
    #   1. KEEP PLAYLIST
    def keep_playlist(self, input_format, query):
        """
        Keep a playlist in the database
        :param input_format: -n (name) -i (id)
        :param query: name or id
        """
        self.pk.keep_playlist(input_format, query)

    @staticmethod
    def help():
        print("youtube2spotify")
        print("keep_playlist")

    # def get_playlist_by_date(self, query, input_format, input_date)
    #     pk = PlaylistKeeper()
    #     pk.get_playlist_by_date(query, input_format, input_date)


def main():
    fire.Fire(MethodManager)


if __name__ == "__main__":
    main()
