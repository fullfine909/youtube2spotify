from fastapi import FastAPI

from sp_manager.method_manager import MethodManager


app = FastAPI()
sp_manager = MethodManager()


@app.get("/")
def home():
    return {"message": "Hello World"}


@app.post("/y2s3/{video_id}")
async def create_sp_playlist_from_youtube_url(video_url: str):
    playlist = sp_manager.ys.youtube_2_spotify(video_url)
    response = {"message": "Playlist created", "playlist": playlist}
    return response


@app.post("/label/{label_name}")
async def create_bp_playlist_from_label(label_name: str):
    playlist = sp_manager.bm.get_top10_label_tracks(label_name)
    response = {"message": "Playlist created", "playlist": playlist}
    return response


# uvicorn src.fastapi.main:app --reload
