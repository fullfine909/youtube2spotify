## How to test it:
### 1. Create a virtual envoirment
`python3 -m venv env`

`source env/bin/activate`
### 2. Install the package
`
python app/setup.py develop
`
### 3. Try a command
`spotify_playlist_manager -m 1 -s https://www.youtube.com/watch?v=vpuHmH495us`

`spotify_playlist_manager -m 2 -s rekids`

`spotify_playlist_manager -m 3 -s planet rhythm`

## Modes:
**Mode 1:** youtube 2 spotify

**Mode 2:** top 10 tracks of a specific label from Beatport

**Mode 3:** full tracklist of a specific label from Beatport
